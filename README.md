# C02 Uhr Klimacamp Bremen
## needet materials:
  - 1 x arduino board
  - 336x WS2811 LEDs
  - RTC e.g. DS3231
  - card board

## how to
  - glue led stripes like this on the card board
  - connect LED-stripe to 5V power supply 
  - connect LEDs and arduino
  - connect RTC
  - flash programm 

### example connections

```
RTC      arduino nano
GND ---- GND
VCC ---- 5V
SQW ---- D3
SCL ---- A5
SDA ---- A4

LED-stripe.data ---- D5
LED-stripe.VCC  ---- 5V
LED-stripe.GND  ---- GND
```
