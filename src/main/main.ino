#include <FastLED.h>
#include <Wire.h>
#include <DS3231.h>

#define INTERRRUPT_PIN 3
#define LED_PIN     5


#define LEDS_PER_SEGMENT    4
#define SEGMENTS_PER_DIGIT  7
#define NUMBER_OF_DIGITS    2
#define NUM_LEDS    LEDS_PER_SEGMENT*SEGMENTS_PER_DIGIT*NUMBER_OF_DIGITS

#define BRIGHTNESS  255
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB

#define DIGIT_COLOR CRGB::DarkCyan

//position of first digit
#define SECONDS  0
#define MINUTES  2  
#define HOURS    4
#define DAYS     6
#define MONTH    8
#define YEARS    10

#define DOOMS_YEAR 27

#define ZERO   0b01111110
#define ONE    0b01000010 
#define TWO    0b00110111
#define THREE  0b01100111
#define FOUR   0b01001011
#define FIFE   0b01101101
#define SIX    0b01111101
#define SEVEN  0b01000110
#define EIGHT  0b01111111
#define NINE   0b01101111
#define NONE   0x00

const uint8_t NUMBERS[] = {ZERO,ONE,TWO,THREE,FOUR,FIFE,SIX,SEVEN,EIGHT,NINE,NONE};

volatile bool newTime = false;

int8_t years_left = 0;
int8_t month_left = 0;
int8_t days_left = 0;
int8_t hours_left = 0;
int8_t min_left = 0;
int8_t seconds_left = 0;

CRGB leds[NUM_LEDS];
RTClib myRTC;
DS3231 ds3231;

void setup() {
    delay( 3000 ); // power-up safety delay
    Wire.begin();
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalSMD5050 );
    FastLED.setBrightness( BRIGHTNESS );
    Serial.begin(9600); // open the serial port at 9600 bps:

    pinMode(INTERRRUPT_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(INTERRRUPT_PIN), tic, FALLING);

    ds3231.enableOscillator(true, false, 0);

    DateTime clock = myRTC.now();
    years_left = DOOMS_YEAR - (uint8_t)(clock.year()- 2000);
    month_left = 12 - clock.month();
    days_left = get_days_of_month(clock.month(), clock.year()) - clock.day();
    hours_left = 24 - clock.hour();
    min_left = 60 - clock.minute();
    seconds_left = 60 - clock.second();  
}


void loop()
{
  if(newTime){
    seconds_left--;
    if(seconds_left<0){
      min_left--;
      seconds_left=59;
    }
    if(min_left<0){
      hours_left--;
      min_left=59;
    }
    if(hours_left<0){
      days_left--;
      hours_left=23;
    }
    if(days_left<0){
      month_left--;
      DateTime clock = myRTC.now();
      years_left = DOOMS_YEAR - (uint8_t)(clock.year()-2000);
      month_left = 12 - clock.month();
      days_left = get_days_of_month(clock.month(), clock.year()) - clock.day();
      hours_left = 24 - clock.hour();
      min_left = 60 - clock.minute();
      seconds_left = 60 - clock.second();  
    }
    set_value_of_digit(seconds_left, SECONDS);
    set_value_of_digit(min_left, MINUTES);
    set_value_of_digit(hours_left, HOURS);
    set_value_of_digit(days_left, DAYS);
    set_value_of_digit(month_left, MONTH);
    set_value_of_digit(years_left, YEARS);

    newTime = false;
  }
}


void tic(){
  newTime = true;
  FastLED.show();
}


void set_value_of_digit(uint8_t value, uint8_t position){
 set_value_at_digit_position(value%10,position+1);
 (value/10 == 0) ?  clear_digit(position) : set_value_at_digit_position(value/10,(position));
}


uint8_t get_days_of_month(uint8_t month, uint16_t year){
  switch (month) {
  case 1: case 3: case 5: case 7: case 8: case 10: case 12:
     return 31;
     break;
  case 2:
    return ((year%4 == 0 && year%100 != 0) || (year%4==0 && year%400==0)) ? 29 : 28;
    break;
  default:
    return 30;
    break;
  }
  return 0;
}

void clear_digit( uint8_t digit){
   set_value_at_digit_position(10,digit);
}

uint16_t get_digit_start_adress( uint8_t digit_number )
{
  return LEDS_PER_SEGMENT * SEGMENTS_PER_DIGIT * digit_number;
}

uint16_t get_segement_start_adress( uint8_t digit_number, uint8_t segement )
{
  return get_digit_start_adress(digit_number) + segement * LEDS_PER_SEGMENT;
}

void set_value_at_digit_position( uint8_t number, uint8_t digit ){
  if(get_digit_start_adress(digit) < NUM_LEDS ){
    for(uint8_t i = 0; i < 7;i++){
      set_leds_of_segment(get_segement_start_adress(digit, i),(NUMBERS[number] & (1<<i)), DIGIT_COLOR);
    }
  }
}

void set_leds_of_segment( uint16_t segment_start, uint8_t status,CRGB color ){
  for(uint16_t i = 0; i < LEDS_PER_SEGMENT;i++){
    if(status != 0){
      leds[segment_start+i]=color;
    }
    else{
      leds[segment_start+i]=CRGB::Black;
    }  
  }
}
